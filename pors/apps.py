from django.apps import AppConfig


class PorsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pors"
